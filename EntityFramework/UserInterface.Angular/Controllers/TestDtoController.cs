﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace UserInterface.Angular.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestDtoController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;

        public TestDtoController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<string> Get()
        {
            using var httpClient = new HttpClient();
            using var response = await httpClient.GetAsync("https://localhost:44383/backend");
            return await response.Content.ReadAsStringAsync();
        }
    }
}