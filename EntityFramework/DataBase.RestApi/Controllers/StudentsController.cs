﻿using System;
using System.Collections.Generic;
using DataBase.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace DataBase.RestApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StudentsController : ControllerBase
    {
        private readonly IStudentRepository studentRepository;

        public StudentsController(IStudentRepository studentRepository)
        {
            this.studentRepository = studentRepository;
        }

        [HttpGet]
        public IReadOnlyCollection<Student> Get()
        {
            return this.studentRepository.Get();
        }
    }
}
