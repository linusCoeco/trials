﻿using DataBase.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace DataBase
{
    public class StudentRepository : IStudentRepository
    {
        public IReadOnlyCollection<Student> Get()
        {
            using var context = new SchoolContext();
            return context.Students.ToArray();
        }
    }
}
