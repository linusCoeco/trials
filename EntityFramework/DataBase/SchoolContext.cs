﻿using DataBase.Contracts;
using System.Data.Entity;

namespace DataBase
{
    public class SchoolContext : DbContext
    {
        public SchoolContext() : base() { }
        public DbSet<Student> Students { get; set; }
        public DbSet<Grade> Grades { get; set; }
    }
}
