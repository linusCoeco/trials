﻿using System.Collections.Generic;

namespace DataBase.Contracts
{
    public interface IStudentRepository
    {
        public IReadOnlyCollection<Student> Get();
    }
}