﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BackEnd.RestApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BackEndController : ControllerBase
    {
        private readonly ILogger<BackEndController> _logger;

        public BackEndController(ILogger<BackEndController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IReadOnlyCollection<TestDto> Get()
        {
            return new List<TestDto> 
            { 
                new TestDto { Name = "Name"} 
            };
        }
    }

    public class TestDto
    {
        public string Name { get; set; }
    }
}
