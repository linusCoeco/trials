﻿using DataBase.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace DataBase.Module
{
    public class DataBaseModule
    {
        public void Register(IServiceCollection collection)
        {
            collection.AddSingleton<IStudentRepository, StudentRepository>();
        }
    }
}
